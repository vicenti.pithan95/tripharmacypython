from django.apps import AppConfig


class TripharmacyappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tripharmacyapp'
