from rest_framework import serializers
from tripharmacyapp.models import Farmacia, Solicitacao, Produto, Marker


class FarmaciaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Farmacia
        fields = '__all__'


class MarkerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Marker
        fields = '__all__'


class ProdutoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Produto
        fields = '__all__'


class SolicitacaoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Solicitacao
        fields = '__all__'
class SolicitacaoUsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Solicitacao
        fields = '__all__'