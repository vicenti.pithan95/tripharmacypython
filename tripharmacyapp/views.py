from django.contrib import messages
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from rest_framework import viewsets, generics
from .models import Farmacia, Marker, Produto, Solicitacao, Funcionario
from .serializer import FarmaciaSerializer, SolicitacaoSerializer, ProdutoSerializer, MarkerSerializer, \
    SolicitacaoUsuarioSerializer
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated


def index(request):
    produtos = Produto.objects.all()
    dados = {
        'usuarios': produtos
    }
    return render(request, 'index.html', dados)


def produto(request):
    if request.user.is_authenticated:
        produtos = Produto.objects.all()
        dados = {
                'produtos': produtos
            }
        if Funcionario.objects.filter(usuario=request.user.id).exists():
            funcionario = Funcionario.objects.filter(usuario=request.user.id).get()
            print(funcionario.usuario.username, 'entrei')
            dados = {
                'funcionario': funcionario,
                'produtos': produtos
            }
            return render(request, 'produto.html',dados)
        return render(request, 'produto.html', dados)
    else:
        return redirect('index')


def go_solicitacao(request, produto_id):
    produtosoli =get_object_or_404(Produto, pk=produto_id)
    produto_solicitado = {
        'produto': produtosoli
    }
    return render(request, 'solicitacao.html',produto_solicitado)


def mysoli(request):
    usuario = User.objects.filter(id=request.user.id).get()
    solicitacao_cliente = list(Solicitacao.objects.filter(usuario=usuario.id))
    x = 0
    total = 0
    while x < len(solicitacao_cliente):
        if (solicitacao_cliente[x].ds_status == 'ABERTA'):
            print(solicitacao_cliente[x].ds_status)
            produto = Produto.objects.filter(id=solicitacao_cliente[x].produto.id).get()
            total += (produto.ds_valor * solicitacao_cliente[x].ds_quantidade)
            print(total)
        x += 1

    usuario_solicitado = {
        'solicitacoes': solicitacao_cliente,
        'total':total
    }
    return render(request, 'usuariosoli.html', usuario_solicitado)

def avaliarsoli(request):
    funcionario = Funcionario.objects.filter(usuario=request.user.id).get()
    produto = list(Produto.objects.filter(farmacia=funcionario.farmacia.id))
    x = 0
    listsoli = []
    num_elementos_lista = len(produto)
    while (x < num_elementos_lista):
        solicitacao_compra_produto = list(Solicitacao.objects.filter(produto=produto[x].id))
        listsoli.extend(solicitacao_compra_produto)
        print(listsoli)
        x += 1

    farmacia_solicitado = {
        'solicitacoes': listsoli
    }
    return render(request, 'avaliarsolicitacao.html',farmacia_solicitado)

def entrega(request):
    if request.method == 'POST':
        solicitacao = request.POST.get('solicitacaoid',0)
        print(solicitacao)
        solicitacao_compra = Solicitacao.objects.filter(id=solicitacao).get()
        if request.user.is_authenticated:
            solicitacao_compra.ds_status = 'FINALIZADO'
            solicitacao_compra.save(update_fields=['ds_status'])
            return redirect('avaliarsoli')
    return redirect('avaliarsoli')


def solicitacao_compra(request):
    if request.method == 'POST':
        produto = request.POST.get('produtoid', 0)
        usuario = User.objects.filter(id=request.user.id).get()
        ds_quantidade = request.POST['quantidade']
        ds_status = 'ABERTA'
        print(produto)
        if request.user.is_authenticated:
            produtoso = Produto.objects.filter(id=produto).get()
            if((produtoso.ds_quantidade_disp -int(ds_quantidade)) >= 0 and int(ds_quantidade) >= 0):
                produtoso.ds_quantidade_disp = (produtoso.ds_quantidade_disp -int(ds_quantidade))
                produtoso.save(update_fields=['ds_quantidade_disp'])
                solicitacaousuario = Solicitacao.objects.create(produto=produtoso, usuario=usuario,
                                                                ds_quantidade=ds_quantidade, ds_status=ds_status)
                solicitacaousuario.save()
                messages.success(request, 'Produto adicionado ao carrinho!')
                return redirect('produto')
        else:
            produto_sem_estoque = True
            retorno= {
                produto_sem_estoque
            }
            return render(request, 'solicitacao.html',retorno)
    return render(request, 'solicitacao.html')


class FarmaciaViewSet(viewsets.ModelViewSet):
    queryset = Farmacia.objects.all()
    serializer_class = FarmaciaSerializer
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]


class SolicitacaoViewSet(viewsets.ModelViewSet):
    queryset = Solicitacao.objects.all()
    serializer_class = SolicitacaoSerializer
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]


class ProdutoViewSet(viewsets.ModelViewSet):
    queryset = Produto.objects.all()
    serializer_class = ProdutoSerializer
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]


class MarkerViewSet(viewsets.ModelViewSet):
    queryset = Marker.objects.all()
    serializer_class = MarkerSerializer
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]


class ListaSolicitacoesUsuario(generics.ListAPIView):
    def get_queryset(self):
        queryset = Solicitacao.objects.filter(usuario=self.kwargs['pk'])
        return queryset

    serializer_class = SolicitacaoUsuarioSerializer
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]
