from django.contrib import admin

from .models import Farmacia, Marker, Solicitacao, Produto, Funcionario

class Solicitacoes(admin.ModelAdmin):
    list_display = ('id','produto','usuario','ds_quantidade','ds_status')
    list_display_links = ('id', 'produto')
    search_fields = ('id','produto__ds_produto','usuario__username')
    list_per_page = 20
admin.site.register(Solicitacao, Solicitacoes)

class Produtos(admin.ModelAdmin):
    list_display = ('id','ds_produto','farmacia','ds_quantidade_disp')
    list_display_links = ('id', 'ds_produto')
    search_fields = ('id','ds_produto','farmacia',)
    list_per_page = 20
admin.site.register(Produto, Produtos)

class Farmacias(admin.ModelAdmin):
    list_display = ('id','ds_farmacia','no_cnpj')
    list_display_links = ('id', 'ds_farmacia')
    search_fields = ('id','ds_farmacia','no_cnpj',)
    list_per_page = 20
admin.site.register(Farmacia, Farmacias)

class Makers(admin.ModelAdmin):
    list_display = ('id','title')
    list_display_links = ('id', 'title')
    search_fields = ('id','tile',)
    list_per_page = 20
admin.site.register(Marker, Makers)

class Funcionarios(admin.ModelAdmin):
    list_display = ('id','usuario','farmacia')
    list_display_links = ('id','usuario','farmacia')
    search_fields = ('id','usuario__username','farmacia__ds_farmacia',)
    list_per_page = 20
admin.site.register(Funcionario, Funcionarios)