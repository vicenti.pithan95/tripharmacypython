from django.urls import path, include
from . import views
from tripharmacyapp.views import FarmaciaViewSet, ProdutoViewSet, SolicitacaoViewSet, MarkerViewSet, ListaSolicitacoesUsuario
from rest_framework import routers
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated

router = routers.DefaultRouter()
router.register('farmacias',FarmaciaViewSet,basename='farmacias')
router.register('produtos',ProdutoViewSet,basename='produtos')
router.register('markers',MarkerViewSet,basename='markers')
router.register('solicitacoes',SolicitacaoViewSet,basename='solicitacoes')

urlpatterns = [
    path('portal/',views.index, name='index'),
    path('portal/produto/',views.produto, name='produto'),
    path('portal/solicitacao/<int:produto_id>',views.go_solicitacao,name='gosolicitacao'),
    path('portal/solicitacao/mysoli',views.mysoli,name='mysoli'),
    path('portal/solicitacao/',views.solicitacao_compra,name='solicitacaocar'),
    path('portal/solicitacao/avaliar_solicitacao/',views.avaliarsoli, name='avaliarsoli'),
    path('portal/solicitacao/entrega/',views.entrega, name='entrega'),
    path('api/',include(router.urls)),
    path('api/usuarios/<int:pk>/solicitacoes/',ListaSolicitacoesUsuario.as_view())
]