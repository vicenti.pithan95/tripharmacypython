# Generated by Django 3.2 on 2021-04-13 01:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tripharmacyapp', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Produtos',
            new_name='Produto',
        ),
    ]
