from django.conf import settings
from django.db import models
from datetime import datetime


class Marker(models.Model):
    title = models.CharField(max_length=45)
    lat = models.IntegerField()
    lng = models.IntegerField()
    snippet = models.CharField(max_length=45)

    def __str__(self):
        return self.title


class Farmacia(models.Model):
    ds_farmacia = models.CharField(max_length=45)
    no_cnpj = models.CharField(max_length=45)
    ds_razao_social = models.CharField(max_length=45)
    maker = models.ForeignKey(Marker, on_delete=models.CASCADE, related_name='marker_id')
    dt_inc = models.DateTimeField(default=datetime.now, blank=True)
    dt_alt = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.ds_farmacia


class Produto(models.Model):
    ds_produto = models.CharField(max_length=45)
    farmacia = models.ForeignKey(Farmacia, on_delete=models.CASCADE, related_name='farmacia_id')
    ds_observacao = models.CharField(max_length=45)
    ds_tipo = models.CharField(max_length=45)
    ds_peso = models.CharField(max_length=45)
    qt_unidade = models.CharField(max_length=45)
    ds_classificacao = models.CharField(max_length=45)
    ds_prescricaomedica = models.BooleanField(default=False)
    ds_quantidade_disp = models.IntegerField()
    ds_valor = models.IntegerField(max_length=45, default=100, blank=True)
    ds_bula = models.CharField(max_length=45)
    foto_produto = models.ImageField(upload_to='fotos/%d/%m/%Y', blank=True)

    def __str__(self):
        return self.ds_produto


class Solicitacao(models.Model):
    produto = models.ForeignKey(Produto, on_delete=models.CASCADE, related_name='produto_id')
    usuario = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='usuario_id')
    ds_quantidade = models.IntegerField()
    ds_status = models.CharField(max_length=45)
    dt_inc = models.DateTimeField(default=datetime.now, blank=True)
    dt_alt = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.ds_status

class Funcionario(models.Model):
    usuario = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='funcionario_id')
    farmacia = models.ForeignKey(Farmacia, on_delete=models.CASCADE, related_name='farmacia')
    ds_observacao = models.CharField(max_length=45, blank=True)

    def __str__(self):
        return self.ds_observacao
