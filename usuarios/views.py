from django.contrib.auth import password_validation
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib import auth, messages
from tripharmacyapp.models import Funcionario


def cadastro(request):
    if request.method == 'POST':
        nome = request.POST['name']
        email = request.POST['email']
        senha = request.POST['pass']
        senha2 = request.POST['pass2']
        if (not email.strip() or not senha.strip() or not senha2.strip() or not nome.strip()):
            print('Algum campo não foi preenchido')
            return redirect('cadastro')
        if senha != senha2:
            messages.error(request, 'As senhas não são iguais')
            print('as senhas não são iguais')
            return redirect('cadastro')
        if User.objects.filter(email=email).exists():
            messages.error(request, 'Usuário ou email já cadastrados')
            print('usuário já cadastrado')
            return redirect('cadastro')
        if User.objects.filter(username=nome).exists():
            print('usuário já cadastrado')
            return redirect('cadastro')
        user = User.objects.create_user(username=nome, email=email, password=senha)
        user.save()
        messages.success(request, 'Usuário cadastrado com sucesso!')
        print('Usuário cadastrado com sucesso!')
        return redirect('login')
    else:
        return render(request, 'usuarios/cadastro.html')


def entrar(request):
    return render(request, 'usuarios/login.html')


def login(request):
    list(messages.get_messages(request))
    if request.method == 'POST':
        email = request.POST['email']
        senha = request.POST['pass']
        if User.objects.filter(email=email).exists():
            nome = User.objects.filter(email=email).values_list('username', flat=True).get()
            user = auth.authenticate(request, username=nome, password=senha)
            if user is not None:
                auth.login(request, user)
                messages.success(request, 'Login realizado com sucesso')
                print('Login realizado com sucesso')
                return redirect('produto')
        else:
            messages.error(request, 'Os dados de acesso estão incorretos')
            list(messages.get_messages(request))
    return render(request, 'usuarios/login.html')


def logout(request):
    auth.logout(request)
    return redirect('login')


def dashboard(request):
    if request.user.is_authenticated:
        if Funcionario.objects.filter(usuario=request.user.id).exists():
            funcionario = Funcionario.objects.filter(usuario=request.user.id).get()
            print(funcionario.usuario.username, 'entrei')
            dados = {
                'funcionario': funcionario
            }
            return render(request, 'usuarios/dashboard.html',dados)
        return render(request, 'usuarios/dashboard.html')
    else:
        return redirect('index')



